<?php

namespace Drupal\ezproxy_stanza\Controller;

use Drupal\node\NodeInterface;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Drupal\ezproxy_stanza\Git\PrivateRepo;

/**
 * Controller for various EZproxy UI routes.
 */
class EZProxyStanza extends ControllerBase implements ContainerInjectionInterface {

  /**
   * Show some basic stats for the config.
   */
  public function display() {
    $elements = [];
    $query = \Drupal::entityQuery('node')
      ->condition('type', 'resource')
      ->condition('status', NodeInterface::PUBLISHED);
    $total = clone $query;
    $elements[] = [
      '#markup' => $this->t('<h2>Some general numbers</h2><strong>Stanzas enabled</strong>: @num', ['@num' => count($total->execute())]),
    ];
    $query->condition('field_ezproxy_review', 1);
    $elements[] = [
      '#markup' => $this->t('<br><strong>Stanzas enabled needing review</strong>: @num', ['@num' => count($query->execute())]),
    ];

    $repo = new PrivateRepo();
    $output = [];
    exec('wc -l ' . $repo->getDirectory() . DIRECTORY_SEPARATOR . 'config.txt', $output);
    $lines = array_pop($output);
    $lines = explode(' ', $lines);
    $elements[] = [
      '#markup' => $this->t('<br><strong>Number of lines in config.txt</strong>: @num', ['@num' => number_format($lines[0])]),
    ];

    $log = explode("\n", $repo->log('--max-count=1'));
    $date = $log[2];
    $date = explode(': ', $date);
    $elements[] = [
      '#markup' => $this->t('<br><strong>Last config update</strong>: @date', ['@date' => $date[1]]),
    ];

    return $elements;
  }

  /**
   * Redirect to the config.txt file.
   */
  public function download() {
    $url = file_create_url(EZPROXY_STANZA_REPO_PRIV . DIRECTORY_SEPARATOR . 'config.txt');

    return new RedirectResponse($url);
  }

  /**
   * Update controller.
   *
   * @todo implement
   */
  public function update() {
    $elements = [];
    $elements[] = [
      '#markup' => '',
    ];
    return $elements;
  }

  /**
   * Pull down changes to private repo.
   */
  public function pull() {
    $repo = new PrivateRepo();
    $repo->pullRemote();

    $response = new Response('1');

    return $response->setPrivate();
  }

  /**
   * View the admin UI.
   */
  public function view() {
    $build = [];
    $build['search'] = [
      '#type' => 'view',
      '#name' => 'ezproxy_stanza_search',
      '#display_id' => 'default',
    ];

    $build['config'] = \Drupal::formBuilder()->getForm('Drupal\ezproxy_stanza\Form\EZProxyStanzaConfigForm');

    return $build;
  }

}
