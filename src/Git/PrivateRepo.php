<?php

namespace Drupal\ezproxy_stanza\Git;

/**
 *
 */
class PrivateRepo extends Git {

  /**
   * Path to the SSH private key.
   *
   * @var string
   */
  private $privateKeyPath;

  /**
   * Construct private repo object.
   */
  public function __construct() {
    parent::__construct(EZPROXY_STANZA_REPO_PRIV, 'priv');
    $this->privateKeyPath = $this->getPrivKeyPath();
    $this->writePrivateKey();
  }

  /**
   *
   */
  public function __destruct() {
  }

  /**
   *
   */
  public function setFileContents($file, $contents, $install = FALSE) {
    if ($file === 'config.txt') {
      $this->setConfig($contents);
    }
    else {
      parent::setFileContents($file, $contents);
    }
  }

  /**
   *
   */
  public function setConfig($top_config = FALSE) {
    if (is_string($top_config)) {
      $top_config = explode("\n", $top_config);
    }
    elseif (!$top_config) {
      $top_config = $this->getFileContents('config.txt');
    }

    while (trim(end($top_config)) === '') {
      array_pop($top_config);
    }

    if (end($top_config) !== EZPROXY_STANZA_CONFIG_TERMINATOR) {
      $top_config[] = '';
      $top_config[] = EZPROXY_STANZA_CONFIG_TERMINATOR;
    }

    $configs = \Drupal::database()->query('SELECT field_ezproxy_stanza_value
      FROM {node__field_ezproxy_stanza} s
      INNER JOIN {node_field_data} n ON n.nid = s.entity_id AND n.status = 1
      LEFT JOIN {node__field_ezproxy_order} o ON o.entity_id = n.nid
      ORDER BY IF(field_ezproxy_order_value, field_ezproxy_order_value, 0), n.title')->fetchCol();

    if (!$this->autoUpdate()) {
      $this->pullRemote();
    }

    $f = fopen($this->getDirectory() . DIRECTORY_SEPARATOR . 'config.txt', 'w');
    if ($f) {
      foreach ($top_config as $config) {
        $config = trim($config);
        fwrite($f, $config);
        fwrite($f, "\n");
      }

      fwrite($f, "\n\n");

      foreach ($configs as $config) {
        $config = trim($config);
        $config = str_replace("\r\n", "\n", $config);
        fwrite($f, $config);
        fwrite($f, "\n\n");
      }

      fclose($f);
    }
    else {
      \Drupal::messenger()->addError('Could not create config.txt');
    }
  }

  /**
   *
   */
  public function pullRemote() {
    $this->writePrivateKey();
    parent::pullRemote();
  }

  /**
   *
   */
  public function updateRemote($msg = 'Update config.txt', $file = 'config.txt') {
    $this->writePrivateKey();
    parent::updateRemote($msg, $file);
  }

  /**
   *
   */
  private function getPrivKeyPath() {
    $info = posix_getpwuid(posix_getuid());
    $path = isset($info['dir']) && is_dir($info['dir']) ? $info['dir'] : \Drupal::service('file_system')->getTempDirectory();
    $path .= DIRECTORY_SEPARATOR . '.ssh' . DIRECTORY_SEPARATOR . 'id_rsa';

    return $path;
  }

  /**
   *
   */
  private function writePrivateKey() {
    $settings = \Drupal::state()->get('ezproxy_stanza_settings');
    if (!empty($settings['authentication']['ssh']['private_key'])) {
      $this->privateKeyPath = $this->getPrivKeyPath();
      if (!file_exists($this->privateKeyPath)) {
        $f = fopen($this->privateKeyPath, 'w');
        if ($f) {
          fwrite($f, $settings['authentication']['ssh']['private_key']);
          fclose($f);
          chmod($this->privateKeyPath, 0700);
        }
      }
      $this->setPrivateKey($this->privateKeyPath);
    }
  }

}
